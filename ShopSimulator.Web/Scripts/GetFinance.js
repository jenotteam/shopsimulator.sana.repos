﻿function GetFinance(getFinanceUrl) {
    this.Start = function () {
        var userId = localStorage.getItem("userId");

        $.post(getFinanceUrl, { id: userId }, function (data) {
            
            $('td.finance.cash').text("$ " + data.Cash);

            $('td.finance.income').text("$ " + data.LastIncome);

            $('td.finance.cost').text("$ " + data.LastCost);

            $('td.finance.total-income').text("$ " + data.TotalIncome);

            $('td.finance.profit').text("$ " + data.Profit);

            $('td.finance.total-cost').text("$ " + data.TotalCost);
        })
    }
}

