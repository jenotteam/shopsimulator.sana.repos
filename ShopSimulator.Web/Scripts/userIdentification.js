﻿
function AddOwner(addOwnerUrl) {
    this.Start = function () {
        var userId = localStorage.getItem("userId");

        if (!userId) {
            userId = guid();
            localStorage.setItem("userId", userId);
            $.post(addOwnerUrl, { userId: userId });
        }
    }
}


function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}