﻿using AutoMapper;
using DataManagement;
using ShopSimulator.Core.Constants;
using ShopSimulator.DataModels;
using ShopSimulator.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopSimulator.Web.Controllers
{
    public class ShopController : Controller
    {
        [HttpGet]
        public ActionResult MainPage()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetShopsByUserId(string id)
        {
            if (String.IsNullOrEmpty(id))
                return HttpNotFound();

            var shops = Framework.ShopDataManager.GetShopsForOwner(id);
            var model = Mapper.Map<List<ShopViewModel>>(shops);

            return PartialView(model);
        }

        [HttpGet]
        public ActionResult AddShop(string ownerId)
        {
            if (String.IsNullOrEmpty(ownerId))
                return HttpNotFound();

            var model = new ShopInputViewModel();

            if (model == null)
                return HttpNotFound();

            model.OwnerId = ownerId;

            model.Sizes = Mapper.Map<List<SizeViewModel>>
                (Framework.FeatureDataManager.GetAllSizes());
            model.ErpList = Mapper.Map<List<FeatureViewModel>>
                (Framework.FeatureDataManager.GetCertainFeatures(FeatureType.Erp));
            model.Addons = Mapper.Map<List<FeatureViewModel>>
                (Framework.FeatureDataManager.GetCertainFeatures(FeatureType.Addon));
            model.Extensions = Mapper.Map<List<FeatureViewModel>>
                (Framework.FeatureDataManager.GetCertainFeatures(FeatureType.Extension));

            return View(model);
        }

        [HttpPost]
        public ActionResult AddShop(ShopInputViewModel model, List<string> erpListSelected, 
            List<string> addonsSelected, List<string> extensionsSelected )
        {
            if (model == null)
                return HttpNotFound();

            if (!ModelState.IsValid)
                return View(model);

            var owner = Framework.OwnerDataManager.GetOwnerById(model.OwnerId);

            var newShop = Mapper.Map<Shop>(model);

            var size = Framework.FeatureDataManager.GetSizeById(newShop.SizeId);

            var unitedIdList = CreateUnitedIdList(erpListSelected, addonsSelected, extensionsSelected);           

            var features = Framework.FeatureDataManager.GetAllFeatures()
                .Where(f => unitedIdList.Contains(f.Id));

            var featureUnitedList = new List<ShopFeature>();

            foreach (var feature in features)
            {
                featureUnitedList.Add(new ShopFeature()
                {
                    ShopId = newShop.Id,
                    FeatureId = feature.Id,
                    TotalTimeDays = feature.Time,
                    TimeDaysLeft = feature.Time,
                });
            }

            newShop.TimeDaysLeft = newShop.TotalTimeDays = featureUnitedList.Sum(m => m.TotalTimeDays) + size.Time;
            newShop.MainPartTimeLeft = size.Time;

            newShop.TotalPrice = features.Sum(m => m.Price) + size.Price;
            newShop.PayForWeek = Math.Round(newShop.TotalPrice / Math.Ceiling(newShop.TotalTimeDays/7.0), 2);

            newShop.FutureIncome = size.Goods;

            foreach (var feature in features)
            {
                newShop.FutureIncome *= feature.GoodsCoefficient;
            }

            newShop.SellingGoods = Math.Round(newShop.FutureIncome, 2);
            newShop.FutureIncome *= CommonConstants.OneItemPrice;

            foreach (var feature in features)
            {
                newShop.FutureIncome *= feature.MarkUp;
            }

            newShop.FutureIncome = Math.Round(newShop.FutureIncome, 2);
            newShop.CreationDate = owner.CurrentDate;           

            Framework.ShopDataManager.AddShop(newShop);
            Framework.FeatureDataManager.AddShopFeatures(featureUnitedList);

            return RedirectToRoute("MainPage");
        }

        [HttpPost]
        public PartialViewResult ShopInfo(string id)
        {
            if (String.IsNullOrEmpty(id))
                return PartialView();

            var items = Framework.ShopDataManager.GetShopFeatures(id);

            var shop = Framework.ShopDataManager.GetShopById(id);

            if (shop == null)
                return PartialView();

            var model = new ShopInfoViewModel();

            model.MainPartTotalTime = shop.Size.Time;
            model.MainPartTimeleft = shop.MainPartTimeLeft;

            foreach (var item in items)
            {
                model.Features.Add(new FeatureProgressViewModel()
                {
                    Name = item.Feature.Name,
                    TotalTimeDays = item.TotalTimeDays,
                    TimeDaysLeft = item.TimeDaysLeft,
                    Type = item.Feature.Type,
                });
            }

            model.Erp = items.Single(f=>f.Feature.Type == FeatureType.Erp).Feature.Name;          

            return PartialView(model);
        }

        List<string> CreateUnitedIdList(List<string> erpListSelected, List<string> addonsSelected, List<string> extensionsSelected)
        {
            var resultList = new List<string>();

            if (erpListSelected == null)
                erpListSelected = new List<string>();

            if (addonsSelected == null)
                addonsSelected = new List<string>();

            if (extensionsSelected == null)
                extensionsSelected = new List<string>();

            resultList.AddRange(erpListSelected);
            resultList.AddRange(addonsSelected);
            resultList.AddRange(extensionsSelected);

            return resultList;
        }
                       
    }
}