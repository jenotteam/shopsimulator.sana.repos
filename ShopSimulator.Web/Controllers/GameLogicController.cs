﻿using DataManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopSimulator.Web.Controllers
{
    public class GameLogicController : Controller
    {
        [HttpPost]
        public void SkipWeek(string ownerId)
        {
            Framework.OwnerDataManager.SkipWeek(ownerId);
        }

        [HttpPost]
        public JsonResult GetWeeks(string ownerId)
        {
            return Json(Framework.OwnerDataManager.GetWeeksByOwnerId(ownerId));
        }
    }
}