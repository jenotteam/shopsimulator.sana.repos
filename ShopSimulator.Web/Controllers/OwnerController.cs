﻿using DataManagement;
using ShopSimulator.Core.Constants;
using ShopSimulator.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopSimulator.Web.Controllers
{
    public class OwnerController : Controller
    {
        [HttpPost]
        public void AddNewOwner(string userId)
        {
            if (!String.IsNullOrEmpty(userId))
                Framework.OwnerDataManager.AddOwner(userId);
        }

        [HttpPost]
        public JsonResult GetOwnersFinance(string id)
        {
            if (String.IsNullOrEmpty(id))
                return null;

            var owner = Framework.OwnerDataManager.GetOwnerById(id);

            if (owner == null)
                return null;
            
            var ownersFinance = new OwnersFinance
            {
                Cash = Math.Round(owner.Cash, 2),
                LastCost = Math.Round(owner.LastCost, 2),
                LastIncome = Math.Round(owner.LastIncome, 2),
                TotalCost = Math.Round(owner.TotalCost, 2),
                TotalIncome = Math.Round(owner.TotalIncome, 2)        
            };

            return Json(ownersFinance);
        }
    }
}