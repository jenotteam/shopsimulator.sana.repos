﻿using AutoMapper;
using DataManagement;
using ShopSimulator.Core.Constants;
using ShopSimulator.DataModels;
using ShopSimulator.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopSimulator.Web.Controllers
{
    public class AdminController : Controller
    {
        [HttpGet]
        public ActionResult EditFeatures()
        {
            var model = new FeaturesEditViewModel();

            model.Sizes = Mapper.Map<List<SizeViewModel>>
                (Framework.FeatureDataManager.GetAllSizes());
            model.ErpList = Mapper.Map<List<FeatureViewModel>>
                (Framework.FeatureDataManager.GetCertainFeatures(FeatureType.Erp));
            model.Addons = Mapper.Map<List<FeatureViewModel>>
                (Framework.FeatureDataManager.GetCertainFeatures(FeatureType.Addon));
            model.Extensions = Mapper.Map<List<FeatureViewModel>>
                (Framework.FeatureDataManager.GetCertainFeatures(FeatureType.Extension));

            return View(model);
        }

        [HttpPost]
        public ActionResult EditFeatures(FeaturesEditViewModel model)
        {
            var sizes = Mapper.Map<List<Size>>(model.Sizes);

            Framework.FeatureDataManager.UpdateSizes(sizes);

            var features = Mapper.Map<List<Feature>>(model.ErpList);
            features.AddRange(Mapper.Map<List<Feature>>(model.Addons));
            features.AddRange(Mapper.Map<List<Feature>>(model.Extensions));

            Framework.FeatureDataManager.UpdateFeatures(features);

            return RedirectToRoute("MainPage");
        }
    }
}