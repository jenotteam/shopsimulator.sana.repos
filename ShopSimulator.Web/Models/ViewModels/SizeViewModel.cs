﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopSimulator.Web.Models.ViewModels
{
    public class SizeViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }

        public int Time { get; set; }

        public int Goods { get; set; }
    }
}