﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopSimulator.Web.Models.ViewModels
{
    public class FeaturesEditViewModel
    {
        public List<SizeViewModel> Sizes { get; set; }

        public List<FeatureViewModel> ErpList { get; set; }

        public List<FeatureViewModel> Addons { get; set; }

        public List<FeatureViewModel> Extensions { get; set; }
    }
}