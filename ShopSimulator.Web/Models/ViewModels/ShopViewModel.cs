﻿using ShopSimulator.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopSimulator.Web.Models.ViewModels
{
    public class ShopViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string OwnerId { get; set; }

        public int CreationDate { get; set; }

        public double LastIncome { get; set; }

        public double LastCost { get; set; }

        public double Income { get; set; }

        public double Cost { get; set; }

        public Size Size { get; set; }

        public int TotalTimeDays { get; set; }

        public int TimeDaysLeft { get; set; }

        public int DaysPasedPercent
        {
            get
            {
                return (int)(100 - (float)this.TimeDaysLeft / this.TotalTimeDays * 100);
            }
        }
        public string ClassForProgressCircle
        {
            get
            {
                return String.Format("c100 p{0} small", DaysPasedPercent);
            }
        }

    }
}