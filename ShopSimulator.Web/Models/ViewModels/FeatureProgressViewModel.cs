﻿using ShopSimulator.Core.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopSimulator.Web.Models.ViewModels
{
    public class FeatureProgressViewModel
    {
        public string Name { get; set; }

        public int TotalTimeDays { get; set; }

        public int TimeDaysLeft { get; set; }

        public FeatureType Type { get; set; }
    }
}