﻿using ShopSimulator.DataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopSimulator.Web.Models.ViewModels
{
    public class ShopInputViewModel
    {
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string OwnerId { get; set; }

        public int CreationDate { get; set; }

        public string SizeId { get; set; }

        public List<SizeViewModel> Sizes { get; set; }

        public List<FeatureViewModel> ErpList { get; set; }

        public List<FeatureViewModel> Addons { get; set; }

        public List<FeatureViewModel> Extensions { get; set; }

        public ShopInputViewModel()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}