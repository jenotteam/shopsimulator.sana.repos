﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopSimulator.Web.Models.ViewModels
{
    public class OwnersFinance
    {
        public double Cash { get; set; }

        public double LastIncome { get; set; }

        public double LastCost { get; set; }

        public double Profit
        {
            get
            {
                return Math.Round(LastIncome - LastCost, 2);
            }
        }

        public double TotalIncome { get; set; }

        public double TotalCost { get; set; }
    }
}