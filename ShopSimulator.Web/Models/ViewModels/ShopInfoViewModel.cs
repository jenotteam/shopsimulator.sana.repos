﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopSimulator.Web.Models.ViewModels
{
    public class ShopInfoViewModel
    {
        public string Erp { get; set; }

        public int MainPartTotalTime { get; set; }

        public int MainPartTimeleft { get; set; }

        public List<FeatureProgressViewModel> Features { get; set; }

        public ShopInfoViewModel()
        {
            Features = new List<FeatureProgressViewModel>();
        }
    }
}