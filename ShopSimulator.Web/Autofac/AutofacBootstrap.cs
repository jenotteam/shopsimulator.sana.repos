﻿using Autofac;
using DataManagement.DataManagers;
using DataManagement.DataProviders;
using DataManagement.DataSqlProviders;
using ShopSimulator.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopSimulator.Web.Autofac
{
    public class AutofacBootstrap
    {
        public static void Run()
        {
            var builder = new ContainerBuilder();            

            builder.RegisterType<FeatureDataManager>().SingleInstance();
            builder.RegisterType<ShopDataManager>().SingleInstance();
            builder.RegisterType<OwnerDataManager>().SingleInstance();

            builder.Register(m => new FeatureDataSqlProvider()).As<IFeatureDataProvider>();
            builder.Register(m => new ShopDataSqlProvider()).As<IShopDataProvider>();
            builder.Register(m => new OwnerDataSqlProvider()).As<IOwnerDataProvider>();

            var container = builder.Build();

            IoC.SetProvider(new AutofacIoCProvider(container));
        }
    }
}