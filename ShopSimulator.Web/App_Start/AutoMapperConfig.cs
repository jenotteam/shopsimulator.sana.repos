﻿using AutoMapper;
using ShopSimulator.DataModels;
using ShopSimulator.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopSimulator.Web.App_Start
{
    public class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.CreateMap<Shop, ShopInputViewModel>();
            Mapper.CreateMap<ShopInputViewModel, Shop>();

            Mapper.CreateMap<Shop, ShopViewModel>();

            Mapper.CreateMap<Size, SizeViewModel>();
            Mapper.CreateMap<SizeViewModel, Size>();

            Mapper.CreateMap<Feature, FeatureViewModel>();
            Mapper.CreateMap<FeatureViewModel, Feature>();
        }
    }
}