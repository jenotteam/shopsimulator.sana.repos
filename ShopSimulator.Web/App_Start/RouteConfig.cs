﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ShopSimulator.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "MainPage",
               url: "home",
               defaults: new { controller = "Shop", action = "MainPage" }
           );

            routes.MapRoute(
                name: "AddShop",
                url: "addshop/{id}",
                defaults: new { controller = "Shop", action = "AddShop", id= UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ShopInfo",
                url: "ShopInfo",
                defaults: new { controller = "Shop", action = "ShopInfo"}
            );

            routes.MapRoute(
                name: "AddNewOwner",
                url: "AddNewOwner",
                defaults: new { controller = "Owner", action = "AddNewOwner" }
            );

            routes.MapRoute(
                name: "GetOwnersFinance",
                url: "GetOwnersFinance",
                defaults: new { controller = "Owner", action = "GetOwnersFinance" }
            );

            routes.MapRoute(
               name: "GetShopsByUserId",
               url: "usershops",
               defaults: new { controller = "Shop", action = "GetShopsByUserId" }
           );

            routes.MapRoute(
               name: "SkipWeek",
               url: "nextweek",
               defaults: new { controller = "GameLogic", action = "SkipWeek"}
           );

            routes.MapRoute(
               name: "GetWeeks",
               url: "getweeks",
               defaults: new { controller = "GameLogic", action = "GetWeeks"}
           );

            routes.MapRoute(
              name: "EditFeatures",
              url: "edit-features",
              defaults: new { controller = "Admin", action = "EditFeatures" }
          );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
