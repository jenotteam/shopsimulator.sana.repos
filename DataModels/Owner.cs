﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopSimulator.DataModels
{
    public class Owner
    {
        public string Id { get; set; }

        public double Cash { get; set; }

        public double TotalIncome { get; set; }

        public double LastIncome { get; set; }

        public double TotalCost { get; set; }

        public double LastCost { get; set; }

        public int CurrentDate { get; set; }

        public virtual ICollection<Week> Weeks { get; set; }

        public Owner()
        {
            Weeks = new List<Week>();
        }
    }
}
