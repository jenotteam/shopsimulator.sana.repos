﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopSimulator.DataModels
{
    public class Shop
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string OwnerId { get; set; }

        public int CreationDate { get; set; }

        public double LastIncome { get; set; }

        public double FutureIncome { get; set; }

        public double LastCost { get; set; }

        public double TotalIncome { get; set; }

        public double SellingGoods { get; set; }

        public double TotalCost { get; set; }

        public int TotalTimeDays { get; set; }

        public int TimeDaysLeft { get; set; }

        public int MainPartTimeLeft { get; set; }

        public double TotalPrice { get; set; }

        public double PayForWeek { get; set; }

        public string SizeId { get; set; }

        public Size Size { get; set; }

        public ICollection<ShopFeature> ShopFeatures { get; set; }

        public Shop()
        {
            ShopFeatures = new List<ShopFeature>();
        }

        public void RecountTime(int daysLeftInWeek)
        {
            if (MainPartTimeLeft > 0)
                MainPartTimeLeft = RecountDaysForFeature(MainPartTimeLeft, ref daysLeftInWeek);

            foreach (var feature in ShopFeatures)
            {
                if (daysLeftInWeek > 0 && feature.TimeDaysLeft > 0)
                    feature.TimeDaysLeft = RecountDaysForFeature(feature.TimeDaysLeft, ref daysLeftInWeek);
                else
                {
                    if (daysLeftInWeek == 0)
                        break;
                }
            }
        }

        int RecountDaysForFeature(int daysToFinishFeature, ref int daysLeftInWeek)
        {
            if (daysToFinishFeature < daysLeftInWeek)
            {
                daysLeftInWeek -= daysToFinishFeature;
                daysToFinishFeature = 0;
            }
            else
            {
                daysToFinishFeature -= daysLeftInWeek;
                daysLeftInWeek = 0;
            }

            return daysToFinishFeature;
        }
    }
}