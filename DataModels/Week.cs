﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopSimulator.DataModels
{
    public class Week
    {
        public string Id { get; set; }

        public double OnlineCash { get; set; }

        public double OfflineCash { get; set; }

        public int Number { get; set; }

        public string OwnerId { get; set; }

        public Owner Owner { get; set; }

        public Week()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}
