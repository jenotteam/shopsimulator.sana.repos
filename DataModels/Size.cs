﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopSimulator.DataModels
{
    public class Size
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }

        public int Goods { get; set; }

        public int Time { get; set; }
    }
}
