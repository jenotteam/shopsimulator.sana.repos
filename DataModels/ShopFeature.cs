﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopSimulator.DataModels
{
    public class ShopFeature
    {
        [Key]
        [Column(Order = 1)]
        public string ShopId { get; set; }

        [Key]
        [Column(Order = 2)]
        public string FeatureId { get; set; }

        public int TotalTimeDays { get; set; }

        public int TimeDaysLeft { get; set; }

        public Shop Shop { get; set; }

        public Feature Feature { get; set; }
    }
}