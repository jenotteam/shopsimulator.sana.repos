﻿using ShopSimulator.Core.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopSimulator.DataModels
{
    public class Feature
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }

        public int Time { get; set; }

        public double MarkUp { get; set; }

        public double GoodsCoefficient { get; set; }

        public FeatureType Type { get; set; }

        public ICollection<ShopFeature> ShopFeatures { get; set; }

        public Feature()
        {
            ShopFeatures = new List<ShopFeature>();
        }
    }
}