﻿using ShopSimulator.Core.Constants;
using ShopSimulator.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement.DataProviders
{
    public interface IFeatureDataProvider
    {
        Size GetSizeById(string id);

        List<Size> GetAllSizes();

        List<Feature> GetCertainFeatures(FeatureType type);

        List<Feature> GetAllFeatures();

        void AddShopFeatures(List<ShopFeature> items);

        void UpdateList<T>(List<T> items);

        void UpdateFeatures(List<Feature> items);

        void UpdateSizes(List<Size> items);
    }
}
