﻿using ShopSimulator.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement.DataProviders
{
    public interface IOwnerDataProvider
    {
        Owner GetOwnerById(string id);

        void AddOwner(Owner owner);

        void UpdateOwner(Owner owner);

        List<Week> GetWeeksByOwnerId(string ownerId);

        void AddWeek(Week week);
    }
}
