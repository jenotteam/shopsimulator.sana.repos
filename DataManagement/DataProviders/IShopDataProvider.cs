﻿using ShopSimulator.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement.DataProviders
{
    public interface IShopDataProvider
    {        
        Shop GetShopById(string id);

        List<Shop> GetShopsForOwner(string id);

        void AddShop(Shop item);

        List<ShopFeature> GetShopFeatures(string id);

        void UpdateShop(Shop shop);
    }
}
