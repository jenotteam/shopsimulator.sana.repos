﻿using DataManagement.DataProviders;
using ShopSimulator.Core;
using ShopSimulator.Core.Constants;
using ShopSimulator.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement.DataManagers
{
    public class FeatureDataManager
    {
        private IFeatureDataProvider DataProvider
        {
            get
            {
                return IoC.Resolve<IFeatureDataProvider>();
            }
        }

        public List<Feature> GetAllFeatures()
        {
            return DataProvider.GetAllFeatures();
        }

        public List<Feature> GetCertainFeatures(FeatureType type)
        {
            return DataProvider.GetCertainFeatures(type);
        }

        public List<Size> GetAllSizes()
        {
            return DataProvider.GetAllSizes();
        }

        public Size GetSizeById(string id)
        {
            return DataProvider.GetSizeById(id);
        }

        public void AddShopFeatures(List<ShopFeature> items)
        {
            DataProvider.AddShopFeatures(items);
        }

        public void UpdateFeatures(List<Feature> items)
        {
            DataProvider.UpdateFeatures(items);
        }

        public void UpdateSizes(List<Size> items)
        {
            DataProvider.UpdateSizes(items);
        }

        public void UpdateList<T>(List<T> items)
        {
            DataProvider.UpdateList(items);
        }
    }
}
