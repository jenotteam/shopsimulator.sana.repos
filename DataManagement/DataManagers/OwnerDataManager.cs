﻿using DataManagement.DataProviders;
using ShopSimulator.Core;
using ShopSimulator.Core.Constants;
using ShopSimulator.DataModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement.DataManagers
{
    public class OwnerDataManager
    {
        private IOwnerDataProvider DataProvider
        {
            get
            {
                return IoC.Resolve<IOwnerDataProvider>();
            }
        }

        public Owner GetOwnerById(string id)
        {
            return DataProvider.GetOwnerById(id);
        }

        public void AddOwner(string id)
        {
            var owner = new Owner()
            {
                Id = id,
                Cash = CommonConstants.StartCash
            };
            DataProvider.AddOwner(owner);
        }        

        public void SkipWeek(string ownerId)
        {
            if (String.IsNullOrEmpty(ownerId))
                return;

            var owner = GetOwnerById(ownerId);

            if (owner == null)
                return;

            var shops = Framework.ShopDataManager.GetShopsForOwner(ownerId);
            owner.LastCost = owner.LastIncome = 0;
            owner.CurrentDate += CommonConstants.OneWeek;

            foreach (var shop in shops)
            {
                shop.ShopFeatures = Framework.ShopDataManager.GetShopFeatures(shop.Id);

                int daysLeftInWeek = CommonConstants.OneWeek;

                if (shop.TimeDaysLeft > 0)
                {
                    if (owner.Cash < shop.PayForWeek)
                        continue;

                    owner.LastCost += shop.PayForWeek;
                    shop.TotalCost += shop.LastCost = shop.PayForWeek;                    

                    shop.TimeDaysLeft -= CommonConstants.OneWeek;

                    if (shop.TimeDaysLeft < 0)
                        shop.TimeDaysLeft = 0;

                    shop.RecountTime(daysLeftInWeek);

                    Framework.FeatureDataManager.UpdateList(shop.ShopFeatures.ToList());
                }
                else
                {
                    if (shop.LastIncome == 0)
                        shop.LastIncome = shop.FutureIncome;

                    owner.LastIncome += shop.LastIncome; 
                    shop.TotalIncome += shop.LastIncome;
                }

                Framework.ShopDataManager.UpdateShop(shop);
            }

            RecountOwnerMoney(owner);
            UpdateOwner(owner);

            AddWeek(new Week()
            {
                OnlineCash = owner.Cash,
                OfflineCash = CommonConstants.StartCash + owner.CurrentDate / CommonConstants.OneWeek * 110,
                Number = owner.CurrentDate / CommonConstants.OneWeek,
                OwnerId = owner.Id
            });
        }

        void RecountOwnerMoney(Owner owner)
        {
            owner.TotalIncome += owner.LastIncome;
            owner.TotalCost += owner.LastCost;
            owner.Cash += owner.LastIncome - owner.LastCost;            
        }

        public List<Week> GetWeeksByOwnerId(string ownerId)
        {
            return DataProvider.GetWeeksByOwnerId(ownerId);
        }

        public void UpdateOwner(Owner owner)
        {
            DataProvider.UpdateOwner(owner);
        }

        public void AddWeek(Week week)
        {
            DataProvider.AddWeek(week);
        }
    }
}
