﻿using DataManagement.DataProviders;
using ShopSimulator.Core;
using ShopSimulator.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement.DataManagers
{
    public class ShopDataManager
    {
        private IShopDataProvider DataProvider
        {
            get
            {
                return IoC.Resolve<IShopDataProvider>();
            }
        }

        public Shop GetShopById(string id)
        {
            return DataProvider.GetShopById(id);
        }

        public void AddShop(Shop item)
        {
            DataProvider.AddShop(item);
        }

        public List<Shop> GetShopsForOwner(string id)
        {
            return DataProvider.GetShopsForOwner(id);
        }

        public List<ShopFeature> GetShopFeatures(string id)
        {
            return DataProvider.GetShopFeatures(id);
        }

        public void UpdateShop(Shop shop)
        {
            DataProvider.UpdateShop(shop);
        }
    }
}
