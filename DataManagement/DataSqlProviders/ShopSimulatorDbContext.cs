﻿using ShopSimulator.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DataManagement
{
    public class ShopSimulatorDbContext : DbContext
    {
        public ShopSimulatorDbContext()
            : base("ShopSimulatorDbConnection")
        { }

        public DbSet<Shop> Shops { get; set; }

        public DbSet<Feature> Features { get; set; }

        public DbSet<Size> Sizes { get; set; }

        public DbSet<ShopFeature> ShopFeatures { get; set; }

        public DbSet<Owner> Owners { get; set; }

        public DbSet<Week> Weeks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
