﻿using DataManagement.DataProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShopSimulator.DataModels;
using ShopSimulator.Core.Constants;
using System.Data.Entity;
using ShopSimulator.Core;

namespace DataManagement.DataSqlProviders
{
    public class FeatureDataSqlProvider : IFeatureDataProvider
    {
        public List<Feature> GetAllFeatures()
        {
            using (var db = new ShopSimulatorDbContext())
            {
                try
                {
                return db.Features.ToList();
            }
                catch
                {
                    Email.Sent("Ошибка работы с БД", "Не удалось доcтать все фичи");
                    return new List<Feature>();
                }
        }
        }

        public List<Feature> GetCertainFeatures(FeatureType type)
        {
            using (var db = new ShopSimulatorDbContext())
            {
                try
                {
                return db.Features
                    .Where(f => f.Type == type)
                    .OrderBy(f => f.MarkUp)
                    .ToList();
            }
                catch
                {
                    Email.Sent("Ошибка работы с БД", "Не удалось доcтать все фичи по типу " + type.ToString());
                    return new List<Feature>();
                }
        }
        }

        public List<Size> GetAllSizes()
        {
            using (var db = new ShopSimulatorDbContext())
            {
                try
                {
                return db.Sizes.ToList();
            }
                catch
                {
                    Email.Sent("Ошибка работы с БД", "Не удалось доcтать размеры для магазина");
                    return new List<Size>();
        }
            }
        }

        public Size GetSizeById(string id)
        {
            if (String.IsNullOrEmpty(id))
                return null;

            using (var db = new ShopSimulatorDbContext())
            {
                try
                {
                return db.Sizes.Single(s => s.Id == id);
            }
                catch
                {
                    Email.Sent("Ошибка работы с БД", "Не удалось доcтать размер для магазина по идентификатору " + id);
                    return null;
        }
            }
        }

        public void AddShopFeatures(List<ShopFeature> items)
        {
            if (items == null)
                return;

            using (var db = new ShopSimulatorDbContext())
            {
                Email.Sent("Ошибка работы с БД", "Не удалось добавить запись в таблицу 'ShopFeatures'");
            }
        }

        public void UpdateFeatures(List<Feature> items)
        {
            if (items == null)
                return;

            using (var db = new ShopSimulatorDbContext())
            {
                try
                {
                foreach (var item in items)
                {
                    db.Entry(item).State = EntityState.Modified;
                }
                db.SaveChanges();
            }
                catch
                {
                    Email.Sent("Ошибка работы с БД", "Не удалось изменить запись в таблице 'Features'");
                }
        }
        }

        public void UpdateSizes(List<Size> items)
        {
            if (items == null)
                return;

            using (var db = new ShopSimulatorDbContext())
            {
                try
                {
                foreach (var item in items)
                {
                    db.Entry(item).State = EntityState.Modified;
                }
                db.SaveChanges();
            }
                catch
                {
                    Email.Sent("Ошибка работы с БД", "Не удалось изменить запись в таблице 'Sizes'");
        }
            }
        }
        
        public void UpdateList<T>(List<T> items)
        {
            if (items == null)
                return;

            using (var db = new ShopSimulatorDbContext())
            {
                try
                {
                    foreach (var item in items)
                {
                    db.Entry((Object)item).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
                catch
                {
                    Email.Sent("Ошибка работы с БД", "Не удалось изменить запись в таблице 'ShopFeatures'");
                }
            }
        }
    }
}
