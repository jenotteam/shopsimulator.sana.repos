﻿using DataManagement.DataProviders;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShopSimulator.DataModels;
using ShopSimulator.Core;

namespace DataManagement.DataSqlProviders
{
    public class ShopDataSqlProvider : IShopDataProvider
    {
        public Shop GetShopById(string id)
        {
            if (String.IsNullOrEmpty(id))
                return null;

            using (var db = new ShopSimulatorDbContext())
            {
                try
                {
                    return db.Shops.Include(s => s.Size).Single(s => s.Id == id);
            }
                catch
                {
                    Email.Sent("Ошибка работы с БД", "Не удалось найти магазин с идентификатором " + id);
                    return null;
                }
        }
        }

        public void AddShop(Shop item)
        {   
            if (item == null)
                return;

            using (var db = new ShopSimulatorDbContext())
            {
                try
                {
                db.Shops.Add(item);
                db.SaveChanges();
            }
                catch
                {
                    Email.Sent("Ошибка работы с БД", "Не удалось добавить новый магазин");
                }
            }
        }

        public List<Shop> GetShopsForOwner(string id)
        {
            if (String.IsNullOrEmpty(id))
                return new List<Shop>();

            using (var db = new ShopSimulatorDbContext())
            {
                try
                {
                return db.Shops.Include(s => s.Size)
                    .Include(s => s.ShopFeatures)
                    .Where(s => s.OwnerId == id)
                    .OrderBy(s => s.CreationDate)
                    .ToList();
            }
                catch
                {
                    Email.Sent("Ошибка работы с БД", "Не удалось доcтать все магазины по владельцу с идентификатором " + id);
                    return new List<Shop>();
                }
            }
        }

        public List<ShopFeature> GetShopFeatures(string id)
        {
            if (String.IsNullOrEmpty(id))
                return new List<ShopFeature>();

            using (var db = new ShopSimulatorDbContext())
            {
                try
                {
                return db.ShopFeatures.Where(f => f.ShopId == id)
                    .Include(f => f.Feature)
                    .ToList();
            }
                catch
                {
                    Email.Sent("Ошибка работы с БД", "Не удалось доcтать все фичи для магазина с идентификатором " + id);
                    return new List<ShopFeature>();
                }
            }
    }

        public void UpdateShop(Shop shop)
        {
            if (shop == null)
                return;

            using (var db = new ShopSimulatorDbContext())
            {
                db.Entry(shop).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
}
}
