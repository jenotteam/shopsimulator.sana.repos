﻿using DataManagement.DataProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShopSimulator.DataModels;
using System.Data.Entity;
using ShopSimulator.Core.Constants;
using ShopSimulator.Core;

namespace DataManagement.DataSqlProviders
{
    public class OwnerDataSqlProvider : IOwnerDataProvider
    {
        public Owner GetOwnerById(string id)
        {
            if (String.IsNullOrEmpty(id))
                return null;

            using (var db = new ShopSimulatorDbContext())
            {
                try
                {
                return db.Owners.Single(o => o.Id == id);
            }
                catch
                {
                    Email.Sent("Ошибка работы с БД", "Не удалось найти владельца магазину по указаному идентификатору");
                    return null;
                }
            }
        }

        public void AddOwner(Owner owner)
        {
            if (owner == null)
                return;

            var week = new Week
            {
                OnlineCash = owner.Cash,
                OfflineCash = owner.Cash,
                Number = 0,
                OwnerId = owner.Id
            };

            using (var db = new ShopSimulatorDbContext())
            {
                try
                {
                db.Owners.Add(owner);
                db.Weeks.Add(week);
                db.SaveChanges();
            }
                catch
                {
                    Email.Sent("Ошибка работы с БД", "Не удалось добавить нового пользователя.");
                }
        }
        }

        public List<Week> GetWeeksByOwnerId(string ownerId)
        {
            using (var db = new ShopSimulatorDbContext())
            {
                try
                {
                return db.Weeks.Where(w => w.OwnerId == ownerId)
                .OrderBy(w => w.Number)
                .ToList();
            }
                catch
                {
                    Email.Sent("Ошибка работы с БД", "Не удалось достать недели для владельца магазина с идентификатором " + ownerId);
                    return new List<Week>();
                }
            }
        }

        public void UpdateOwner(Owner owner)
        {
            using (var db = new ShopSimulatorDbContext())
            {
                try
                {
                db.Entry(owner).State = EntityState.Modified;
                db.SaveChanges();
            }
                catch
                {
                    Email.Sent("Ошибка работы с БД", "Не удалось редактировать владельца магазина с идентификатором " + owner.Id);
        }
            }
        }

        public void AddWeek(Week week)
        {
            using (var db = new ShopSimulatorDbContext())
            {
                try
                {
                db.Weeks.Add(week);
                db.SaveChanges();
            }
                catch
                {
                    Email.Sent("Ошибка работы с БД", "Не удалось добавить новую запись в таблицу 'Weeks'");
                }
            }
        }
    }
}

