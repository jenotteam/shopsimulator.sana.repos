﻿using DataManagement.DataManagers;
using ShopSimulator.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataManagement
{
    public class Framework
    {
        public static ShopDataManager ShopDataManager
        {
            get
            {
                return IoC.Resolve<ShopDataManager>();
            }
        }

        public static FeatureDataManager FeatureDataManager
        {
            get
            {
                return IoC.Resolve<FeatureDataManager>();
            }
        }

        public static OwnerDataManager OwnerDataManager
        {
            get
            {
                return IoC.Resolve<OwnerDataManager>();
            }
        }
    }
}
