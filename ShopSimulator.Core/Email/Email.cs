﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace ShopSimulator.Core
{
    public class Email
    {
        public static void Sent(string subject, string body, string recipient = "555v.1.t.y.o.k555@gmail.com")
        {
            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress("connectiondefault@gmail.com", "Student-Portal");
                mail.To.Add(recipient);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                using (SmtpClient smtp = new SmtpClient())
                {
                    smtp.EnableSsl = true;
                    smtp.Send(mail);
                }
            }
        }        
    }
}