﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopSimulator.Core.Constants
{
    public static class Roles
    {
        public const string Admin = "Admin";

        public const string Owner = "Owner";
    }
}