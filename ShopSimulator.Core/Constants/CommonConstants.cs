﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopSimulator.Core.Constants
{
    public static class CommonConstants
    {
        public const string FinanceHasNotGotten = "0,0,0";

        public const int OneWeek = 7;

        public const int OneItemPrice = 1;

        public const int StartCash = 13000;
    }

    public static class ErpConstants
    {
        public const string Nav = "NAV";

        public const string SapEcc = "SAP ECC";

        public const string SapBusinessOne = "SAP B1";

        public const string Ax = "AX";

        public const string CustomErp = "Custom ERP";
    }

    public enum FeatureType { Erp, Addon, Extension }
}