﻿using Autofac;
using ShopSimulator.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopSimulator.Core
{
    public class AutofacIoCProvider : IIoCProvider
    {
        private IContainer container;

        public AutofacIoCProvider(IContainer container)
        {
            this.container = container;
        }

        public T Resolve<T>()
        {
            return this.container.Resolve<T>();
        }
    }
}