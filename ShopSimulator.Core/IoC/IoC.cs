﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopSimulator.Core
{
    public static class IoC
    {
        public static IIoCProvider provider;

        public static void SetProvider(IIoCProvider provider)
        {
            IoC.provider = provider;
        }

        public static T Resolve<T>()
        {
            return provider.Resolve<T>();
        }
    }

    public interface IIoCProvider
    {
        T Resolve<T>();
    }
}
